# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Zygmunt Krynicki
VERSION 0.7
PROJECT zygoon/groundwork

FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
RUN --mount type=cache,target=/var/cache/apt apt-get update && apt-get install --no-install-recommends -y build-essential xz-utils lzip curl patch

RUN mkdir -p /dl /src /build
RUN adduser --home /home/ci --shell /bin/sh ci
RUN chown ci /dl /src /build
ENV PATH=/opt/groundwork/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

IMPORT ./m4
IMPORT ./sed
IMPORT ./ed
IMPORT ./bc
IMPORT ./autoconf
IMPORT ./automake
IMPORT ./libtool
IMPORT ./texinfo
IMPORT ./bison
IMPORT ./gmp
IMPORT ./mpfr
IMPORT ./mpc

deps:

world:
	BUILD m4+package
	BUILD sed+package
	BUILD ed+package
	BUILD bc+package
	BUILD autoconf+package
	BUILD automake+package
	BUILD libtool+package
	BUILD texinfo+package
	BUILD bison+package
	BUILD gmp+package
	BUILD mpfr+package
	BUILD mpc+package
