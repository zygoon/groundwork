<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki
-->

# About Groundwork

Groundwork is an experiment to build a minimal set of user-space programs, from
scratch, using containerized environment as the build system. Groundwork
started as a very very long `Dockerfile` and has since evolved to a small tree
of `Earthlyfile`s. 
