# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Zygmunt Krynicki
VERSION 0.7
IMPORT ./.. AS groundwork

FROM groundwork+deps

USER ci

ARG --global PN=sed
ARG --global PV=4.9
ARG --global PEXT=tar.xz

fetch:
	WORKDIR /dl
	RUN curl --remote-name http://ftp.gnu.org/gnu/$PN/$PN-$PV.$PEXT

unpack:
	FROM +fetch
	WORKDIR /src
	RUN tar -axf /dl/$PN-$PV.$PEXT
	WORKDIR src/$PN-$PV

patch:
	FROM +unpack

configure:
	FROM +patch
	WORKDIR /build/$PN-$PV
	RUN /src/$PN-$PV/configure --prefix=/opt/groundwork

compile:
	FROM +configure
	RUN make -j $(nproc)

check:
	FROM +compile
	RUN make check -j $(nproc)

install:
	FROM +check
	RUN make install DESTDIR=/tmp/stage

package:
	FROM +install
	RUN tar -zcf $PN.native.tar.gz -C /tmp/stage .
	SAVE ARTIFACT $PN.native.tar.gz
